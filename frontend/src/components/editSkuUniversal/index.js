import React from "react";
import app from "../../common/app";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import swal from "sweetalert";
import { Dropdown } from "react-bootstrap";
import config from "../../common/config";

class EditSkuUniversal extends React.Component {
  constructor() {
    super();
    this.state = {
      updated: {},
      inserted: {},
      deleted: [],
      fetched: {},
    };
  }

  componentDidMount() {
    const editSkuUniversal = async () => {
      const queryParams = new URLSearchParams(window.location.search);
      const sku = queryParams.get("sku");
      let resp = await app.editSkuUniversal(sku);
      if (resp.data.meta.retval === 1) {
        this.setState({ fetched: resp.data.data[0] });
      }
    };

    editSkuUniversal();
  }

  handleChange = (e) => {
    const { name, value, valueAsNumber } = e.target;
    const { fetched, updated, inserted } = this.state;
    let deletedRecord = this.state.deleted;

    // Value already existed so added inside updated object
    if (fetched[name] || name === "Notes") {
      if (value || name === "Notes") {
        // While updating remove the items from delete array
        // Delete record
        const index = deletedRecord.indexOf(name);
        if (index > -1) {
          // only splice array when item is found
          deletedRecord.splice(index, 1); // 2nd parameter means remove one item only
        }
        this.setState({
          ...this.state,
          updated: {
            ...updated,
            [name]: isNaN(Number(value))
              ? value
              : valueAsNumber
              ? valueAsNumber
              : "",
          },
          deleted: deletedRecord,
        });
      } else {
        // Push the name inside the delete array. As it has been deleted
        if (name !== "Notes") {
          deletedRecord.push(name);
        }

        // Remove the updated value from updated array
        let newUpdated = { ...updated };
        delete newUpdated[name];
        this.setState({
          ...this.state,
          updated: {
            ...newUpdated,
          },
          deleted: deletedRecord,
        });
      }
    } else {
      // New value is to be inserted
      if (value) {
        this.setState({
          ...this.state,
          inserted: {
            ...inserted,
            [name]: isNaN(Number(value)) ? value : valueAsNumber,
          },
        });
      } else {
        // Remove the value from inserted object
        let newInsert = { ...inserted };
        delete newInsert[name];
        this.setState({
          ...this.state,
          inserted: {
            ...newInsert,
          },
        });
      }
    }
  };

  handleDropdown = (e) => {
    const { name, textContent } = e.target;
    const { updated, fetched } = this.state;
    this.setState({
      ...this.state,
      updated: {
        ...updated,
        [name]: textContent,
      },
      fetched: {
        ...fetched,
        [name]: textContent,
      },
    });
  };

  handleSubmit = (e) => {
    let updateSkuPromise,
      deleteSkuPromise,
      insertSkuPromise = null;
    const { updated, inserted, deleted, fetched } = this.state;
    if (Object.keys(updated).length) {
      let { Notes, StatusName, ...assortmentValues } = updated;
      if (
        Notes ||
        Notes === "" ||
        Object.keys(assortmentValues).length ||
        StatusName
      ) {
        let payload = {
          assortmentValues: Object.entries(assortmentValues).map((e) => ({
            [e[0]]: e[1],
          })),
          sku: fetched?.SKU,
          Notes: Notes || Notes === "" ? Notes : fetched.Notes,
          StatusName: StatusName ? StatusName : fetched.StatusName,
        };
        updateSkuPromise = app.updateSkuDetails(payload);
      }
    }

    if (deleted.length) {
      let payload = {
        assortmentValues: deleted.filter((dl) => dl != "Notes"),
        sku: fetched.SKU,
        Notes: deleted.includes("Notes"),
      };
      deleteSkuPromise = app.deleteSkuDetails(payload);
    }

    if (Object.keys(inserted).length) {
      let { Notes, ...assortmentValues } = inserted;
      if (Notes || Object.keys(assortmentValues).length) {
        let payload = {
          assortmentValues: Object.entries(assortmentValues).map((e) => ({
            [e[0]]: e[1],
          })),
          sku: fetched?.SKU,
          Notes: Notes ? Notes : fetched.Notes,
        };
        insertSkuPromise = app.insertSkuDetails(payload);
      }
    }

    Promise.all([updateSkuPromise, insertSkuPromise, deleteSkuPromise]).then(
      (resp) => {
        console.log(resp);
        if (
          resp[0]?.data.meta.retval === 1 ||
          resp[1]?.data.meta.retval === 1 ||
          resp[2]?.data.meta.retval === 1
        ) {
          swal({
            text: "Records successfully updated",
            icon: "success",
            button: "Ok",
          }).then(() => {
            window.location.href = "/por/universal";
          });
        }
      }
    );
  };

  render() {
    let editKeys = Object.keys(this.state.fetched);
    return (
      <div className="d-flex justify-content-center">
        <div 
          className="w-50"
          style={{
            backgroundColor: "#eeeeee",
            padding: "10px",
            margin: "30px",
          }}
        >
          <div className="edit-header">
            <i
              style={{ cursor: "pointer" }}
              className="fa fa-arrow-left ms-2"
              aria-hidden="true"
              onClick={() => (window.location.href = "/por/universal")}
            ></i>
            <p>EDIT</p>
          </div>
          {editKeys?.map((det) => {
            const values = this.state.fetched[det];
            return (
              <>
                {det === "StatusName" ? (
                  <>
                    <Form.Label className="mt-3">{det}</Form.Label> 
                    <Dropdown>
                      <Dropdown.Toggle
                        id="dropdown-button-dark-example1"
                        // variant="dark"
                        className="dropdown-color"
                      >
                        {this.state.fetched.StatusName}
                      </Dropdown.Toggle>

                      <Dropdown.Menu variant="light">
                        {config.STATUS_NAMES.map((el, i) => (
                          <Dropdown.Item
                            key={i}
                            name="StatusName"
                            onClick={(e) => this.handleDropdown(e)}
                          >
                            {el}
                          </Dropdown.Item>
                        ))}
                      </Dropdown.Menu>
                    </Dropdown>
                  </>
                ) : (
                  <>
                    <Form.Label className="mt-3">{det}</Form.Label>
                    <Form.Control
                      type={det === "Notes" ? "text" : "number"}
                      onChange={(e) => this.handleChange(e)}
                      // value={values}
                      name={det}
                      disabled={det === "SKU" ? true : false}
                      defaultValue={values}
                      onWheel={(e) => e.currentTarget.blur()}
                    />
                  </>
                )}
              </>
            );
          })}
          <Button
            onClick={(e) => this.handleSubmit(e)}
            className="my-3 submit_style"
            variant="primary"
          >
            Submit
          </Button>
        </div>
      </div>
    );
  }
}

export default EditSkuUniversal;
