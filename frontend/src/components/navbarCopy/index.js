import React from "react";
import { Container, Form, InputGroup, Nav, Navbar } from "react-bootstrap";
import { useLocation, useNavigate } from "react-router";
import config from "../../common/config";
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";

function NavbarComp({
  handleSearchInput,
  handleSearchDd,
  dropdownValue,
  searchInput,
}) {
  const navigate = useNavigate();
  const location = useLocation();
  let pathName = location.pathname;
  let tabName = "/por";
  if (location?.pathname !== "/por/addSku") {
    if (pathName === "/por" || pathName === "/por/edit") {
      tabName = "/por";
    } else if (
      pathName === "/por/protection" ||
      pathName === "/por/protection/edit"
    ) {
      tabName = "/por/protection";
    } else if (
      pathName === "/por/universal" ||
      pathName === "/por/universal/edit"
    ) {
      tabName = "/por/universal";
    }
  } else if (location?.pathname === "/por/addSku") {
    if (location.state?.tabName === "/por") {
      tabName = "/por";
    } else if (location.state?.tabName === "/por/protection") {
      tabName = "/por/protection";
    } else if (location.state?.tabName === "/por/universal") {
      tabName = "/por/universal";
    }
  }

  return (
    <Navbar sticky="top" variant="light">
      <Container fluid="lg">
        <Navbar.Brand href="/por">
          <img
            style={{ maxHeight: "2.8rem" }}
            src="https://www.t-mobile.com/content/dam/t-mobile/ntm/branding/logos/corporate/tmo-logo-v4.svg"
            alt="T-mobile"
          />
        </Navbar.Brand>
        <Navbar.Brand style={{ fontWeight: "600" }} href="/por">
          PLAN OF RECORD
        </Navbar.Brand>
        <Nav className="me-auto">
          <Nav.Link
            className={`me-3 ${
              tabName === "/por" ? "button-dark" : "button-light"
            }`}
            onClick={() => navigate("/por")}
          >
            Device
          </Nav.Link>
          <Nav.Link
            className={`me-3 ${
              tabName === "/por/protection" ? "button-dark" : "button-light"
            }`}
            onClick={() => navigate("/por/protection")}
          >
            Protection
          </Nav.Link>
          <Nav.Link
            className={`me-3 ${
              tabName === "/por/universal" ? "button-dark" : "button-light"
            }`}
            onClick={() => navigate("/por/universal")}
          >
            Universal
          </Nav.Link>
        </Nav>

        {config.MENUS.includes(window.location.pathname) ? (
          <InputGroup className="navbar-search">
            <DropdownButton
              title={dropdownValue}
              id="input-group-dropdown-1"
              className="button-dark"
            >
              {config.SEARCH_DROPDOWN_NEW[location.pathname].map((el) => {
                return (
                  <Dropdown.Item onClick={() => handleSearchDd(el)} href="#">
                    {el}
                  </Dropdown.Item>
                );
              })}
            </DropdownButton>
            <Form.Control
              aria-label="Default"
              aria-describedby="inputGroup-sizing-default"
              placeholder="Search SKU..."
              // id="myInput"
              // onKeyUp={searchBySku}
              value={searchInput === null ? "" : searchInput}
              onChange={(e) => handleSearchInput(e)}
            />
            <InputGroup.Text className="search-icon">
              <i className="fas fa-search"></i>
            </InputGroup.Text>
          </InputGroup>
        ) : null}
      </Container>
    </Navbar>
  );
}

export default NavbarComp;
