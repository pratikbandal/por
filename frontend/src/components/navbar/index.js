import React, { useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import app from "../../common/app";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import Logo from "../../assets/images/Logo.jpg";

const NavbarComp = () => {
  const navigate = useNavigate();
  const [search, setSearch] = useState("");

  const handleChange = (e) => {
    const { value } = e.target;
    setSearch(value);
  };

  // Submitting search
  const handleSearch = async () => {
    let payload = {
      search: search,
    };
    let searchPromise = await app.searchTable(payload);
  };
  const { state } = useLocation();
  return (
    <>
      <Navbar className="navbar_por" expand="lg">
        <Container fluid>
          {/* <Navbar.Brand style={{cursor: "pointer", color: 'white'}} onClick={() => navigate("/")}>POR Tool</Navbar.Brand> */}
          <Navbar.Toggle aria-controls="navbarScroll" />
          <Navbar.Collapse id="navbarScroll">
            <Nav
              className="me-auto lg-0"
              style={{ marginBottom: "-2rem" }}
              navbarScroll
            >
              {/* <Nav.Link onClick={() => navigate("/protection")}>Protection</Nav.Link>
            <Nav.Link onClick={() => navigate("/universal")}>Universal</Nav.Link> */}

              <ul class="nav nav-tabs nav-tabs-por">
                <li class="nav-item tab-hover">
                  <span
                    style={{ cursor: "pointer" }}
                    onClick={() => navigate("/")}
                    class={`nav-link ${
                      window.location.pathname === "/" || state?.tabName === "/"
                        ? "active nav-font-text"
                        : "nav-items_text"
                    }`}
                    aria-current="page"
                  >
                    Device
                  </span>
                </li>
                <li class="nav-item tab-hover">
                  <span
                    style={{ cursor: "pointer" }}
                    onClick={() => navigate("/protection")}
                    class={`nav-link ${
                      window.location.pathname === "/protection" ||
                      state?.tabName === "/protection"
                        ? "active nav-font-text"
                        : "nav-items_text"
                    }`}
                  >
                    Protection
                  </span>
                </li>
                <li class="nav-item tab-hover">
                  <span
                    style={{ cursor: "pointer" }}
                    onClick={() => navigate("/universal")}
                    class={`nav-link ${
                      window.location.pathname === "/universal" ||
                      state?.tabName === "/universal"
                        ? "active nav-font-text"
                        : "nav-items_text"
                    }`}
                  >
                    Universal
                  </span>
                </li>
              </ul>
            </Nav>
            <div>
              <h4
                style={{
                  color: "white",
                  marginRight: "219px",
                  fontSize: "31px",
                  marginTop: "15px",
                  fontWeight: "700",
                }}
              >
                PLAN OF RECORD
              </h4>
            </div>
            <Form action={false} className="d-flex">
              <Form.Control
                type="search"
                placeholder="Search here...."
                className="me-2 search-nav"
                aria-label="Search"
                name="search"
                // value={search}
                // onChange={(e) => handleChange(e)}
                id="myInput"
              />
              {/* <Button onClick= {() => handleSearch()} variant="outline-success">Search</Button> */}
            </Form>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <div className="logo_image">
        <img style={{ width: "230px", height: "100px" }} src={Logo}></img>
      </div>
    </>
  );
};

export default NavbarComp;
