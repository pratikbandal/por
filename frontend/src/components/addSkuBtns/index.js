import React from "react";
import { useNavigate } from "react-router-dom";
import * as XLSX from "xlsx";
import * as FileSaver from "file-saver";
import { Button } from "react-bootstrap";

const AddSkuBtns = () => {
  const navigate = useNavigate();
  let tabName = window.location.pathname;
  const fileType =
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
  const fileExtension = ".xlsx";

  const ExportToExcel = (type, fileName) => {
    var csvData = document.getElementById("tbl_exporttable_to_xls");
    const ws = XLSX.utils.table_to_sheet(csvData);
    const wb = { Sheets: { data: ws }, SheetNames: ["data"] };
    const excelBuffer = XLSX.write(wb, { bookType: "xlsx", type: "array" });
    const data = new Blob([excelBuffer], { type: fileType });
    FileSaver.saveAs(data, fileName ? fileName : "device" + fileExtension);
  };

  return (
    <div className="mt-5">
      <Button
        onClick={() => navigate("/por/addSku", { state: { tabName: tabName } })}
        type="button"
        className="button-dark me-3"
      >
        Add SKU
      </Button>
      <Button
        onClick={() => ExportToExcel("xlsx", tabName.replace("/", ""))}
        type="button"
        className="button-dark"
      >
        Excel download
      </Button>
    </div>
  );
};

export default AddSkuBtns;
