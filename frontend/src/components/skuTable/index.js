import React, { useEffect, useState } from "react";
import Table from "react-bootstrap/Table";
import { useNavigate } from "react-router-dom";
import $ from "jquery";
import config from "../../common/config";
import EditTable from "../editTable";
import TableBody from "../TableBody";

const SkuTable = ({ data, dataHead, deleteSku, fetchTableData }) => {
  const navigate = useNavigate();
  const [edit, setEdit] = useState(false);
  const [columnName, setColumnName] = useState(null);

  useEffect(() => {
    let dropdownMenuEl = document.querySelector(".filter-menu");
    let filterEl = document.querySelector(".fa-filter");
    window.addEventListener("click", (e) => {
      if (dropdownMenuEl) {
        if (dropdownMenuEl.style.display === "block" && e.target !== filterEl) {
          dropdownMenuEl.style.display = "none";
        }
      }
    });

    return () => {
      window.removeEventListener("click", () => {});
    };
  }, []);

  const handleDropdown = () => {
    let dropdownMenuEl = document.querySelector(".filter-menu");
    dropdownMenuEl.style.display = "block";
  };

  const filterLockgroup = (value) => {
    $("#myTable tr").filter(function () {
      $(this).toggle(
        $(this).text().toLowerCase().indexOf(value.toLowerCase()) > -1
      );
      return null;
    });
    let dropdownMenuEl = document.querySelector(".filter-menu");
    dropdownMenuEl.style.display = "none";
  };

  const renderFilter = (data) => {
    let filterDropdown = null;
    if (data === "Lockgroup") {
      filterDropdown = (
        <>
          <i
            style={{ cursor: "pointer", color: "rgb(226, 0, 116)" }}
            className="fa fa-filter ms-2"
            aria-hidden="true"
            onClick={() => handleDropdown()}
          ></i>
          <div className="dropdown-menu filter-menu">
            {config.FILTER_LOCKGROUP.map((dd, i) => {
              let name = dd;
              if (dd === "Clear Filter") {
                dd = "";
              }
              return (
                <span
                  key={i}
                  style={{ cursor: "pointer" }}
                  onClick={() => filterLockgroup(dd)}
                  className="dropdown-item"
                >
                  {name}
                </span>
              );
            })}
          </div>
        </>
      );
    }

    return filterDropdown;
  };

  const renderTableHead = () => {
    let tableHead = <></>;

    tableHead = (
      <tr>
        <th className="header" scope="col">
          Action
        </th>
        {dataHead &&
          dataHead.map((data, i) => {
            return (
              <th className="header" scope="col" key={i}>
                {data}
                {renderFilter(data)}
                {data.toString().includes("202") || data.toString().includes("Notes") || data.toString().includes("StatusName") ? (
                  <EditTable
                    onEditClick={onEditClick}
                    data={data}
                    index={i}
                    editMode={edit}
                    columnName={columnName}
                    fetchTableData={fetchTableData}
                  />
                ) : null}
              </th>
            );
          })}
      </tr>
    );
    return tableHead;
  };

  const renderTableAction = (item) => {
    if (item) {
      return (
        <td>
          <div className="d-flex align-items-center action_buttons">
            <span
              onClick={() =>
                navigate(`edit?sku=${item.SKU}`, {
                  state: { tabName: window.location.pathname },
                })
              }
            >
              <i
                className="fa fa-pencil-square-o edit-icon"
                aria-hidden="true"
              ></i>
            </span>
            <i
              onClick={() => deleteSku(item.SKU)}
              className="fa fa-trash delete-icon"
              aria-hidden="true"
            ></i>
          </div>
        </td>
      );
    } else {
      return null;
    }
  };

  const onEditClick = (index, data) => {
    setEdit(index);
    setColumnName(data);
  };

  return (
    <Table
      id="tbl_exporttable_to_xls"
      responsive
      size="md"
      hover
      bordered
      striped
    >
      <thead
        style={{ position: "sticky", top: "0", height: "50px", zIndex: "1000" }}
        className="table-head"
      >
        {renderTableHead(data)}
      </thead>

      <tbody id="myTable">
        {data?.map((item, i) => {
          let dataValue = Object.values(item);
          return (
            <tr key={i}>
              {renderTableAction(item)}
              {/* {renderTableBody(dataValue)} */}
              <TableBody
                dataValue={dataValue}
                index={edit}
                data={columnName}
                fetchTableData={fetchTableData}
                rawData={item}
              />
            </tr>
          );
        })}
        {!data.length && (
          <tr>
            <td colSpan={dataHead?.length + 1}>
              <span id="table-no-results">No results found</span>
            </td>
          </tr>
        )}
      </tbody>
    </Table>
  );
};

export default SkuTable;
