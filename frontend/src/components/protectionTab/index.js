import React, { useEffect, useState } from "react";
import app from "../../common/app";
import AddSkuBtns from "../addSkuBtns";
import SkuTable from "../skuTable";
import swal from "sweetalert";

const ProtectionTab = ({ searchInput, dropdownValue, handleSearchDd }) => {
  const [state, setState] = useState({
    protectionData: [],
    protectionTableHead: [],
    goAhead: false,
    filteredProtectionData: [],
  });

  const fetchTableData = async () => {
    let resp = await app.fetchProtectionTable();
    setState({
      protectionData: resp.data.list,
      protectionTableHead: Object.keys(resp.data.list[0]),
      goAhead: true,
      filteredProtectionData: resp.data.list,
    });
  };

  useEffect(() => {
    fetchTableData();
    handleSearchDd("Any");
  }, []);

  const deleteSku = async (skuId) => {
    let tabName = "/por";
    if (window.location.pathname === "/") {
      tabName = "/por";
    } else if (window.location.pathname.includes("/por/protection")) {
      tabName = "/por/protection";
    } else if (window.location.pathname.includes("/por/universal")) {
      tabName = "/por/universal";
    }

    swal({
      title: "Are you sure?",
      text: `You want to delete the SKU ${skuId}.\n For deleting multiple SKUS contact administrator`,
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then(async (willDelete) => {
      if (willDelete) {
        let skuDelete = await app.deleteSku(skuId);
        if (skuDelete && skuDelete.data.meta.retval === 1) {
          swal({
            text: "Record successfully deleted",
            icon: "success",
            button: "Ok",
          }).then(() => {
            window.location.href = tabName;
          });
        }
      }
    });

  };

  useEffect(() => {
    if (searchInput === null) return;

    const searchTableData = () => {
      let filteredProtectionData = app.searchDropdown(
        state.protectionData,
        dropdownValue,
        searchInput
      );
      setState((prevState) => ({
        ...prevState,
        filteredProtectionData: filteredProtectionData,
        protectionTableHead: state.protectionTableHead,
        goAhead: true,
      }));
    };

    searchTableData();
  }, [searchInput, state.protectionData, state.protectionTableHead]);

  return (
    <>
      <AddSkuBtns />

      {state.goAhead && (
        <SkuTable
          data={state.filteredProtectionData}
          dataHead={state.protectionTableHead}
          deleteSku={deleteSku}
          fetchTableData={fetchTableData}
        />
      )}
    </>
  );
};

export default ProtectionTab;
