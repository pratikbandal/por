import React, { useEffect, useState } from "react";
import Dropdown from "react-bootstrap/Dropdown";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { useLocation, useNavigate } from "react-router-dom";
import app from "../../common/app";
import FloatingLabel from "react-bootstrap/esm/FloatingLabel";
import swal from "sweetalert";
import Card from "react-bootstrap/Card";

const AddSku = () => {
  const { state } = useLocation();
  const [lockGroup, setLockGroup] = useState("LockGroup");
  const [lockGroupList, setLockGroupList] = useState([]);
  const [sku, setSku] = useState("");
  const navigate = useNavigate();

  useEffect(() => {
    const getDropdownList = async () => {
      try {
        let lockGroup = await app.getLockgrouplist();
        console.log(lockGroup.data.list);
        setLockGroupList(lockGroup.data.list);
      } catch (e) {
        console.log(e);
      }
    };

    if (state?.tabName === "/por" || state?.tabName === "/por/" ) {
      getDropdownList();
    }

    return () => { };
  }, [state]);

  const handleDropdown = (e) => {
    setLockGroup(e.target.textContent);
  };

  const handleChange = (e) => {
    setSku(e.target.value.replace(/\n/g, " "));
  };

  // let isDisabled =
  //   (lockGroup !== "LockGroup" && (state?.tabName !== "/por" || state?.tabName !== "/por/")) && sku
  //     ? false
  //     : true;

  let isDisabled = false;
  if (state?.tabName === "/por" || state?.tabName === "/por/") {
    if ((lockGroup !== "LockGroup") && sku) {
      isDisabled = false;
    }
    else {
      isDisabled = true;
    }
  } else {
    if (sku) {
      isDisabled = false;
    }
    else {
      isDisabled = true;
    }
  }


  const removeExtraSpace = (sku) => {
    console.log(sku)
    let removedSpaces = sku.split(" ").filter((sk) => sk !== "");
    console.log(removedSpaces);
    return removedSpaces;
  };

  const addValidSku = async (skusPresent) => {
    let payload = {
      skus: skusPresent,
      tabName: state?.tabName,
      lockGroup: lockGroup !== "Lockgroup" ? lockGroup : null,
    };
    let resp = await app.addSku(payload);
    if (resp.data && resp.data.meta.retval === 1) {
      swal({
        text: "Records successfully added",
        icon: "success",
        button: "Ok",
      }).then(() => {
        navigate(state?.tabName);
      });
    }
    // SKU already exists
    else if (resp.data && resp.data.meta.retval === 2) {
      swal({
        text: resp.data.meta.message,
        icon: "error",
        button: "Ok",
      });
    } else if (resp.data && resp.data.meta.retval === 3) {
      swal({
        text: resp.data.meta.message,
        icon: "error",
        button: "Ok",
      });
    }
  };

  const handleAddSku = async () => {
    try {
      let formattedSkus = sku.includes(",")
        ? sku.split(",")
        : removeExtraSpace(sku);

      let resp = await app.searchSkuBWMaterial(formattedSkus);

      if (resp.data && resp.data.meta.retval === 1) {
        let skusPresent = resp.data.skusPresent;
        let skusNotPresent = resp.data.skusNotPresent;
        swal(
          `
          ${skusNotPresent.length
            ? "SKUS " + skusNotPresent + " not present"
            : ""
          }
          ${skusPresent.length > 1
            ? "Do you still want to proceed to add these SKUS " +
            skusPresent +
            " ?"
            : skusPresent.length === 1
              ? "Do you still want to proceed to add these SKU " +
              skusPresent +
              " ?"
              : ""
          }
        `,
          skusPresent.length
            ? {
              buttons: {
                cancel: "Cancel",
                yes: {
                  text: "Yes",
                  value: "yes",
                },
              },
            }
            : { buttons: false, dangerMode: true, icon: "warning" }
        ).then((value) => {
          switch (value) {
            case "yes":
              addValidSku(skusPresent);
              break;

            default:
              console.log("Cancel");
          }
        });
      }
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <div className="mt-4 p-4">
      {/* Note for adding skus */}
      <Card>
        <Card.Header>
          <strong>Note: </strong>For adding multiple SKU, seperate the SKU by
          comma.
        </Card.Header>
      </Card>
      {(state?.tabName === "/por" || state?.tabName === "/por/") && (
        <div className="d-flex align-items-center mt-4">
          <div className="me-2">LockGroup: </div>
          <div className="lockgroup_dropdown">
            <Dropdown>
              <Dropdown.Toggle
                id="dropdown-button-dark-example1"
                // variant="light"
                style={{
                  backgroundColor: "rgb(226, 0, 116)",
                  boxShadow: "none",
                }}
              >
                {lockGroup}
              </Dropdown.Toggle>

              <Dropdown.Menu>
                {lockGroupList.map((lockGroup, i) => (
                  <Dropdown.Item key={i} onClick={(e) => handleDropdown(e)}>
                    {lockGroup}
                  </Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
          </div>
        </div>
      )}

      <div className="mt-3 w-50">
        <FloatingLabel controlId="floatingInput" label="Enter SKU">
          <Form.Control
            aria-label="Small"
            aria-describedby="inputGroup-sizing-sm"
            placeholder="Enter SKU"
            type="text"
            name="sku"
            onChange={(e) => handleChange(e)}
            value={sku}
            as="textarea"

            style={{ height: '150px' }}
          />
        </FloatingLabel>
      </div>

      <div className="mt-3">
        <Button
          disabled={isDisabled}
          onClick={handleAddSku}
          className="submit_style"
        >
          Submit
        </Button>{" "}
      </div>
    </div>
  );
};

export default AddSku;
