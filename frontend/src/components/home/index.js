import React, { useEffect, useState } from "react";
import app from "../../common/app";
import AddSkuBtns from "../addSkuBtns";
import SkuTable from "../skuTable";
import swal from "sweetalert";

const Home = ({ searchInput, dropdownValue, handleSearchDd }) => {
  const [state, setState] = useState({
    deviceData: [],
    deviceTableHead: [],
    goAhead: false,
    filteredDeviceData: [],
  });

  const deleteSku = async (skuId) => {
    let tabName = "/por";
    if (window.location.pathname === "/") {
      tabName = "/por";
    } else if (window.location.pathname.includes("/por/protection")) {
      tabName = "/por/protection";
    } else if (window.location.pathname.includes("/por/universal")) {
      tabName = "/por/universal";
    }
   
    
      swal({
        title: "Are you sure?",
        text: `You want to delete the SKU ${skuId}.\n For deleting multiple SKUS contact administrator`,
        icon: "warning",
        buttons: true,
        dangerMode: true,
      }).then(async(willDelete) => {
        if (willDelete) {
          let skuDelete = await app.deleteSku(skuId);
          if (skuDelete && skuDelete.data.meta.retval === 1) {
            swal({
              text: "Record successfully deleted",
              icon: "success",
              button: "Ok",
            }).then(() => {
              window.location.href = tabName;
            });
          } 
        }
      });
    
  };

  const fetchTableData = async () => {
    let resp = await app.fetchTableData();
    setState((prevState) => ({
      ...prevState,
      deviceData: resp.data.list,
      deviceTableHead: Object.keys(resp.data.list[0]),
      goAhead: true,
      filteredDeviceData: resp.data.list,
    }));
  };

  useEffect(() => {
    fetchTableData();
    handleSearchDd("Any");
  }, []);

  useEffect(() => {
    if (searchInput === null) return;

    const searchTableData = () => {
      let filteredDeviceData = app.searchDropdown(
        state.deviceData,
        dropdownValue,
        searchInput
      );
      setState((prevState) => ({
        ...prevState,
        filteredDeviceData: filteredDeviceData,
        // deviceTableHead: state.deviceTableHead,
        // goAhead: true,
      }));
    };

    searchTableData();
  }, [searchInput, state.deviceData, state.deviceTableHead, dropdownValue]);

  return (
    <>
      {/* Add Sku buttons */}
      <AddSkuBtns />

      {/* Sku table */}
      {state.goAhead && (
        <SkuTable
          data={state.filteredDeviceData}
          dataHead={state.deviceTableHead}
          deleteSku={deleteSku}
          fetchTableData={fetchTableData}
        />
      )}
    </>
  );
};

export default Home;
