import React, { lazy, useEffect, useState } from "react";
import { Route, Routes } from "react-router-dom";
import { Loader } from "../loader";
import Navbar from "../navbarCopy";

const Home = lazy(() => import(/* webpackChunkName: 'home' */ "../home"));
const AddSku = lazy(() => import(/* webpackChunkName: 'addSku' */ "../addSku"));
const ProtectionTab = lazy(() =>
  import(/* webpackChunkName: 'protectionTab' */ "../protectionTab")
);
const UniversalTab = lazy(() =>
  import(/* webpackChunkName: 'universalTab' */ "../universalTab")
);
const EditSku = lazy(() =>
  import(/* webpackChunkName: 'editSku' */ "../editSku")
);
const EditSkuProtection = lazy(() =>
  import(/* webpackChunkName: 'editSkuProtection' */ "../editSkuProtection")
);
const EditSkuUniversal = lazy(() =>
  import(/* webpackChunkName: 'editSkuUniversal' */ "../editSkuUniversal")
);

const RoutesComp = () => {
  const [searchInput, setSearchInput] = useState(null);
  const [dropdownValue, setDropdownValue] = useState("");

  const handleSearchInput = (e) => {
    const { value } = e.target;
    setSearchInput(value);
  };

  const handleSearchDd = (value) => {
    setDropdownValue(value);
    setSearchInput("");
  };
  
  return (
    <>
      <Navbar
        handleSearchInput={handleSearchInput}
        handleSearchDd={handleSearchDd}
        dropdownValue={dropdownValue}
        searchInput={searchInput}
      />
    <div class="lds-facebook d-none"><div></div><div></div><div></div></div>
      <Routes>
        <Route
          path="/por"
          key={0}
          element={
            <React.Suspense fallback={<Loader />}>
              <Home
                handleSearchDd={handleSearchDd}
                searchInput={searchInput}
                dropdownValue={dropdownValue}
              />
            </React.Suspense>
          }
        />
        <Route
          path="/por/addSku"
          key={1}
          element={
            <React.Suspense fallback={<Loader />}>
              <AddSku />
            </React.Suspense>
          }
        />
        <Route
          path="/por/protection"
          key={2}
          element={
            <React.Suspense fallback={<Loader />}>
              <ProtectionTab
                handleSearchDd={handleSearchDd}
                searchInput={searchInput}
                dropdownValue={dropdownValue}
              />
            </React.Suspense>
          }
        />
        <Route
          path="/por/universal"
          key={3}
          element={
            <React.Suspense fallback={<Loader />}>
              <UniversalTab
                handleSearchDd={handleSearchDd}
                searchInput={searchInput}
                dropdownValue={dropdownValue}
              />
            </React.Suspense>
          }
        />
        <Route
          path="/por/edit"
          key={4}
          element={
            <React.Suspense fallback={<Loader />}>
              <EditSku />
            </React.Suspense>
          }
        />
        <Route
          path="/por/protection/edit"
          key={5}
          element={
            <React.Suspense fallback={<Loader />}>
              <EditSkuProtection />
            </React.Suspense>
          }
        />
        <Route
          path="/por/universal/edit"
          key={5}
          element={
            <React.Suspense fallback={<Loader />}>
              <EditSkuUniversal />
            </React.Suspense>
          }
        />
      </Routes>
    </>
  );
};

export default RoutesComp;
