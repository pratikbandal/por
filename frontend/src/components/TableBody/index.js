import React from "react";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import Popover from "react-bootstrap/Popover";
import app from "../../common/app";
import debounce from "lodash.debounce";
import config from "../../common/config";
import { Dropdown } from "react-bootstrap";

function TableBody({ dataValue, index, i, data, fetchTableData, rawData }) {

  const popover = (skuNumber, materialStandardName) => (
    <Popover id="popover-basic">
      <Popover.Header as="h3">SKU number</Popover.Header>
      <Popover.Body>{skuNumber}</Popover.Body>
      <Popover.Header as="h3">Material standard name</Popover.Header>
      <Popover.Body>{materialStandardName}</Popover.Body>
    </Popover>
  );

  const handleChange = async (e, skuNumber, prevValue) => {
    let { value } = e.target;
    if (prevValue) {
      // Update operation
      if (value) {
        let payload = {
          value: e.target.value,
          sku: skuNumber,
          date: data,
        };
        await app.updateAssortment(payload);
      } else {
        // Delete operation
        // alert("Delete operation")
        let payload = {
          sku: skuNumber,
          date: data,
        };
        await app.deleteAssortment(payload);
        fetchTableData();
      }
    }
    // Insert operation
    else {
      if (value) {
        let payload = {
          value: value,
          sku: skuNumber,
          date: data,
        };
        await app.insertAssortment(payload);
      } else {
        // For deleting 0
        let payload = {
          sku: skuNumber,
          date: data,
        };
        await app.deleteAssortment(payload);
        fetchTableData();
      }
    }

  };

  const handleDropdown = async (e, skuNumber) => {
    let payload = {
      value: e.target.textContent,
      sku: skuNumber,
      date: data,
    };
    await app.updateAssortment(payload);
    fetchTableData();
  }

  const renderInput = (value, skuNumber, materialStandardName) => {
    return (
      data === "StatusName" ? (
        <div className="d-flex align-items-center">
          <OverlayTrigger
            rootClose
            trigger="hover"
            placement="left"
            overlay={popover(skuNumber, materialStandardName)}
          >
            <i className="fa fa-info-circle me-2"></i>
          </OverlayTrigger>
          <Dropdown>
            <Dropdown.Toggle
              id="dropdown-button-dark-example1"
              className="dropdown-color"
            >
              {value}
            </Dropdown.Toggle>

            <Dropdown.Menu variant="light">
              {config.STATUS_NAMES.map((el, i) => (
                <Dropdown.Item
                  key={i}
                  name="StatusName"
                  onClick={(e) => handleDropdown(e, skuNumber)}
                >
                  {el}
                </Dropdown.Item>
              ))}
            </Dropdown.Menu>
          </Dropdown>
        </div>
      )
        :
        <InputGroup className="mb-3 d-flex align-items-center">
          <OverlayTrigger
            rootClose
            trigger="hover"
            placement="left"
            overlay={popover(skuNumber, materialStandardName)}
          >
            <i className="fa fa-info-circle me-2"></i>
          </OverlayTrigger>
          <Form.Control
            aria-label="Username"
            aria-describedby="basic-addon1"
            defaultValue={value}
            onChange={(e) =>
              debounce(() => handleChange(e, skuNumber, value), 1000)()
            }
            type={data === "Notes" ? "text" : "number"}
          />
          {/* <i
        style={{ cursor: "pointer", color: "rgb(226, 0, 116)" }}
        className="fa fa-check ms-2"
        aria-hidden="true"
        onClick={() => handleSubmit(skuNumber, )}
      ></i> */}
        </InputGroup>
    );
  };
  const renderTableBody = (dataValue, index, rawData) => {
    let skuNumber = dataValue[0];
    let materialStandardName = rawData["MaterialStandardName"];
    let nodeBody = dataValue.map((dt, i) => {
      if (index === i) {
        let inputStyle = ""
        if (data === "Notes") {
          inputStyle = {
            style: {
              minWidth: "480px"
            }
          }
        }

        return <td {...inputStyle} key={i}>{renderInput(dt, skuNumber, materialStandardName)}</td>;
      } else {
        return <td key={i}>{dt}</td>;
      }
    });

    return nodeBody;
  };
  return renderTableBody(dataValue, index, rawData);
}

export default TableBody;
