import React from "react";

function EditTable({
  onEditClick,
  data,
  index,
  editMode,
  columnName,
  fetchTableData,
}) {

  const handleSubmit = () => {
    onEditClick(false, data);
    fetchTableData();
  }

  const handleEdit = () => {
    onEditClick(index, data)
    // fetchTableData();
  }
  return (
    <>
      {!editMode ? (
        <i
          style={{ cursor: "pointer", color: "rgb(226, 0, 116)" }}
          className="fa fa-pencil ms-2"
          aria-hidden="true"
          onClick={() => handleEdit()}
        ></i>
      ) : (
        columnName === data && (
          <i
            style={{ cursor: "pointer", color: "rgb(226, 0, 116)" }}
            className="fa fa-check ms-2"
            aria-hidden="true"
            onClick={() => handleSubmit()}
          ></i>
        )
      )}
    </>
  );
}

export default EditTable;
