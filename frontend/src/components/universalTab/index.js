import React, { useEffect, useState } from "react";
import app from "../../common/app";
import AddSkuBtns from "../addSkuBtns";
import SkuTable from "../skuTable";
import swal from "sweetalert";

const UniversalTab = ({ searchInput, dropdownValue, handleSearchDd }) => {
  const [state, setState] = useState({
    universalData: [],
    universalTableHead: [],
    goAhead: false,
    filteredUniData: [],
  });

  const fetchTableData = async () => {
    let resp = await app.fetchUniversalTable();
    setState({
      universalData: resp.data.list,
      universalTableHead: Object.keys(resp.data.list[0]),
      goAhead: true,
      filteredUniData: resp.data.list,
    });
  };

  useEffect(() => {
    fetchTableData();
    handleSearchDd("Any");
  }, []);

  useEffect(() => {
    if (searchInput === null) return;

    const searchTableData = () => {
      let filteredUniData = app.searchDropdown(
        state.universalData,
        dropdownValue,
        searchInput
      );
      setState((prevState) => ({
        ...prevState,
        filteredUniData: filteredUniData,
        universalTableHead: state.universalTableHead,
        goAhead: true,
      }));
    };

    searchTableData();
  }, [searchInput, state.universalData, state.universalTableHead]);

  const deleteSku = async (skuId) => {
    let tabName = "/por";
    if (window.location.pathname === "/por") {
      tabName = "/por";
    } else if (window.location.pathname.includes("/por/protection")) {
      tabName = "/por/protection";
    } else if (window.location.pathname.includes("/por/universal")) {
      tabName = "/por/universal";
    }
    
      swal({
        title: "Are you sure?",
        text: `You want to delete the SKU ${skuId}.\n For deleting multiple SKUS contact administrator`,
        icon: "warning",
        buttons: true,
        dangerMode: true,
      }).then(async(willDelete) => {
        if (willDelete) {
          let skuDelete = await app.deleteSku(skuId);
          if (skuDelete && skuDelete.data.meta.retval === 1) {
            swal({
              text: "Record successfully deleted",
              icon: "success",
              button: "Ok",
            }).then(() => {
              window.location.href = tabName;
            });
          }
        }
      });
    
  };

  return (
    <>
      <AddSkuBtns />

      {state.goAhead && (
        <SkuTable
          data={state.filteredUniData}
          dataHead={state.universalTableHead}
          deleteSku={deleteSku}
          fetchTableData={fetchTableData}
        />
      )}
    </>
  );
};

export default UniversalTab;
