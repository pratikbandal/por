import axios from 'axios';

let core = {};
window.apiCalls = 0;

core.makeAPICall = (param) => {
  if (!param.url) return false;
  const sNodeSource = ".lds-facebook";
  const sNodeTarget = "d-none";

  const testLoaderVisibility = () => {
    --window.apiCalls;
    if (window.apiCalls <= 0) {
      const isRemove = false;
      core.addClassToElement(document, sNodeSource, sNodeTarget, isRemove);
      window.apiCalls = 0;
    }
  };

  param.credentials = "include";
  param.method = param.method || "GET";
  param.data = param.data || {};
  // param.headers = config.headers;
  ++window.apiCalls;

  if (param.loader !== false) {
    const isRemove = true;
    core.addClassToElement(document, sNodeSource, sNodeTarget, isRemove);
  }

  let axiosPromise = axios(param);
  axiosPromise
    .then(() => {
      testLoaderVisibility();
    })
    .catch(() => {
      testLoaderVisibility();
    });
  return axiosPromise;
};

core.addClassToElement = (objDocumentP, sNodeSource, sNodeTarget, isRemove) => {
  const objDocument = objDocumentP ? objDocumentP : document;
  let $bodyEle =
    objDocument && objDocument.querySelector
      ? objDocument.querySelector(sNodeSource)
      : null;
  if ($bodyEle && $bodyEle.classList) {
    if (!$bodyEle.classList.contains(sNodeTarget)) {
      $bodyEle.classList.add(sNodeTarget);
    }
    if (isRemove) {
      $bodyEle.classList.remove(sNodeTarget);
    }
  }
};

export default core;
