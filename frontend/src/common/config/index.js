let config = {};
config.URL_BASE = "/";

config.enumStaticUrls = {
  home: "/",
};

// Development env.
if (window.location.hostname.includes("localhost")) {
  config.BASE_DOMAIN = "http://localhost:3006/";
  config.API_BASE = config.BASE_DOMAIN + "portool/api/";
  console.log("dev");
}
// Production env.
else {
  config.BASE_DOMAIN = "http://10.77.103.136:3006/";
  config.API_BASE = config.BASE_DOMAIN + "portool/api/";
}

config.FILTER_LOCKGROUP = [
  "Clear Filter",
  "Accessories",
  "IPHONE",
  "SUPER",
  "AFFORDABLE",
  "WEAR_UNIV",
  "TABLET",
  "HOTSPOT",
  "MID-SUPER",
  "DURABLE",
  "PHONE_FIRST",
  "IOT",
  "AFFORDABLE_HIGH",
  "NETWORK",
];

config.DEVICE_SET = ["GOOGLE ORIOLE"];

config.STATUS_NAMES = [
  "Complete",
  "Update Needed",
  "Contingency",
  "Expired",
  "Cancelled",
  "Cancelled - SAS update Needed",
  "Part of Baseline Forecast",
];

config.MENUS = ["/por", "/por/", "/por/protection", "/por/universal"];

config.SEARCH_DROPDOWN = ["Any", "SKU", "MaterialStandardName", "StatusName"];

config.SEARCH_DROPDOWN_NEW = {
  "/por/protection": [
    "Any",
    "SKU",
    "MaterialStandardName",
    "Department",
    "DeviceSet",
    "Class",
    "StatusName",
  ],
  "/por": [
    "Any",
    "SKU",
    "Lockgroup",
    "SubCategory",
    "MaterialStandardName",
    "DetailDescription",
    "StatusName",
  ],
  "/por/": [
    "Any",
    "SKU",
    "Lockgroup",
    "SubCategory",
    "MaterialStandardName",
    "DetailDescription",
    "StatusName",
  ],
  "/por/universal": [
    "Any",
    "SKU",
    "Department",
    "Class",
    "SubClass",
    "MaterialStandardName",
    "StatusName",
  ],
};

export default config;
