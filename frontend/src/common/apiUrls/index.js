import config from "../config"

export const getLockgrouListUrl = config.API_BASE + "device/lockgroup/list";

export const searchTableUrl = config.API_BASE + "search/table";

export const fetchTableDataUrl = config.API_BASE + "device/table/list";

export const searchTableDataUrl = config.API_BASE + "device/search/table"

export const protectionDataUrl = config.API_BASE + "protection/table/list";

export const universalDataUrl = config.API_BASE + "universal/table/list";

export const addSkuUrl = config.API_BASE + "add/sku";

export const searchBWSkuUrl = config.API_BASE + "search/sku/bwmaterial";

export const deleteSkuUrl = config.API_BASE + "delete/sku";

export const editSkuUrl = config.API_BASE + "device/sku";

export const editSkuProtectionUrl = config.API_BASE + "protection/sku";

export const editSkuUniversalUrl = config.API_BASE + "universal/sku";

export const updateSkuUrl = config.API_BASE + "update/sku";

export const updateAssortmentUrl = config.API_BASE + "update/assortment/sku";

export const insertAssortmentUrl = config.API_BASE + "insert/assortment/sku";

export const deleteAssortmentUrl = config.API_BASE + "delete/assortment/sku";

export const updateSkuDetailsUrl = config.API_BASE + "update/sku/details";

export const deleteSkuDetailsUrl = config.API_BASE + "delete/sku/details";

export const insertSkuDetailsUrl = config.API_BASE + "insert/sku/details";