import {
    addSkuUrl,
    deleteAssortmentUrl,
    deleteSkuDetailsUrl,
    deleteSkuUrl,
    editSkuProtectionUrl,
    editSkuUniversalUrl,
    editSkuUrl,
    fetchTableDataUrl,
    getLockgrouListUrl,
    insertAssortmentUrl,
    insertSkuDetailsUrl,
    protectionDataUrl,
    searchBWSkuUrl,
    searchTableDataUrl,
    searchTableUrl,
    universalDataUrl,
    updateAssortmentUrl,
    updateSkuDetailsUrl,
    updateSkuUrl,
  } from "../apiUrls";
  import core from "../core";
  
  let app = {};
  
  app.getLockgrouplist = () => {
    let params = {
      url: getLockgrouListUrl,
    };
  
    return core.makeAPICall(params);
  };
  
  app.searchTable = (payload) => {
    let params = {
      url: searchTableUrl,
      params: payload,
    };
  
    return core.makeAPICall(params);
  };
  
  app.fetchTableData = () => {
    let params = {
      url: fetchTableDataUrl,
      loader: true,
    };
  
    return core.makeAPICall(params);
  };
  
  app.searchTableData = (payload) => {
    let params = {
      url: searchTableDataUrl,
      loader: true,
      params: {
        search: payload,
      },
    };
  
    return core.makeAPICall(params);
  };
  
  app.fetchProtectionTable = () => {
    let params = {
      url: protectionDataUrl,
      loader: true,
    };
  
    return core.makeAPICall(params);
  };
  
  app.fetchUniversalTable = () => {
    let params = {
      url: universalDataUrl,
      loader: true,
    };
  
    return core.makeAPICall(params);
  };
  
  app.addSku = (payload) => {
    let params = {
      url: addSkuUrl,
      loader: true,
      method: "post",
      data: payload,
    };
  
    return core.makeAPICall(params);
  };

  app.searchSkuBWMaterial = (payload) => {
    let params = {
      url: searchBWSkuUrl,
      loader: true,
      method: "get",
      params: {skus: payload},
    };
  
    return core.makeAPICall(params);
  };
  
  app.deleteSku = (userId) => {
    let params = {
      url: deleteSkuUrl,
      loader: true,
      method: "delete",
      params: {
        sku: userId,
      },
    };
  
    return core.makeAPICall(params);
  };
  
  app.editSku = (userId) => {
    let params = {
      url: editSkuUrl,
      loader: true,
      method: "get",
      params: {
        sku: userId,
      },
    };
  
    return core.makeAPICall(params);
  };
  
  app.editSkuProtection = (userId) => {
    let params = {
      url: editSkuProtectionUrl,
      loader: true,
      method: "get",
      params: {
        sku: userId,
      },
    };
  
    return core.makeAPICall(params);
  };
  
  app.editSkuUniversal = (userId) => {
    let params = {
      url: editSkuUniversalUrl,
      loader: true,
      method: "get",
      params: {
        sku: userId,
      },
    };
  
    return core.makeAPICall(params);
  };
  
  app.updateSku = (payload) => {
    let params = {
      url: updateSkuUrl,
      loader: true,
      method: "post",
      data: payload,
    };
  
    return core.makeAPICall(params);
  };
  
  app.updateAssortment = (payload) => {
    let params = {
      url: updateAssortmentUrl,
      method: "put",
      data: payload,
      loader: false,
    };
  
    return core.makeAPICall(params);
  };
  
  app.insertAssortment = (payload) => {
    let params = {
      url: insertAssortmentUrl,
      method: "post",
      data: payload,
      loader: false,
    };
  
    return core.makeAPICall(params);
  };
  
  app.deleteAssortment = (payload) => {
    let params = {
      url: deleteAssortmentUrl,
      method: "delete",
      data: payload,
      loader: false,
    };
  
    return core.makeAPICall(params);
  };
  
  app.updateSkuDetails = (payload) => {
    let params = {
      url: updateSkuDetailsUrl,
      method: "put",
      data: payload,
      loader: true,
    };
  
    return core.makeAPICall(params);
  };
  
  app.deleteSkuDetails = (payload) => {
    let params = {
      url: deleteSkuDetailsUrl,
      method: "delete",
      data: payload,
      loader: true,
    };
  
    return core.makeAPICall(params);
  };
  
  app.insertSkuDetails = (payload) => {
    let params = {
      url: insertSkuDetailsUrl,
      method: "post",
      data: payload,
      loader: true,
    };
  
    return core.makeAPICall(params);
  };
  
  app.searchDropdown = (deviceData, dropdownValue, searchInput) => {
    return deviceData.filter((dt) => {
      if (dropdownValue === "Any") {
        return (
          Object.values(dt)
            .toString()
            .toLowerCase()
            .includes(searchInput.toLowerCase())
        );
      } else if (dt[dropdownValue]) {
        return dt[dropdownValue]
          .toLowerCase()
          .includes(searchInput.toLowerCase());
      }
    });
  };
  
  export default app;
  