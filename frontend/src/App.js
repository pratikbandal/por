import './App.css';
import RoutesComp from './components/routes';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="container-fluid">
      <RoutesComp />
    </div>
  );
}

export default App;
