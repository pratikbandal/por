const addMultipleSku = {
  tags: ["Default"],
  description: "Add multiple skus",
  requestBody: {
    content: {
      "application/json": {
        schema: {
          type: "object",
          properties: {
            skus: {
              type: "array",
              description: "List of skus",
              example: ["194253124030"],
            },
            tabName: {
              type: "string",
              description: "Tab name",
              example: "device",
            },
            lockGroup: {
              type: "string",
              description: "Lock group",
              example: "IPHONE",
            },
          },
        },
      },
    },
  },
  responses: {
    201: {
      description: "OK",
      content: {
        "application/json": {
          schema: {
            type: "object",
            example: {
              success: true,
              data: "Sku added",
              meta: {
                retval: 1,
                serverTime: new Date(),
              },
            },
          },
        },
      },
    },
    409: {
      description: "SKU already exists",
      content: {
        "application/json": {
          schema: {
            type: "object",
            example: {
              success: false,
              data: null,
              meta: {
                message: "SKU already exists",
                retval: 2,
                serverTime: new Date(),
              },
            },
          },
        },
      },
    },
  },
};

const deleteSku = {
  tags: ["Default"],
  description: "For deleting sku",
  parameters: [
    {
      name: "sku",
      in: "query",
      description: "sku to be deleted",
      type: " string",
      example: "194253124030",
    },
  ],
  responses: {
    200: {
      description: "SKU deleted",
      content: {
        "application/json": {
          schema: {
            type: "object",
            example: {
              success: true,
              data: "SKU deleted",
              meta: {
                retval: 1,
                serverTime: new Date(),
              },
            },
          },
        },
      },
    },
  },
};

const updateAssortmentSku = {
  tags: ["Default"],
  description: "Update assortment value for the given sku",
  requestBody: {
    content: {
      "application/json": {
        schema: {
          type: "object",
          properties: {
            sku: {
              type: "string",
              description: "SKU",
              example: "194253124030",
            },
            date: {
              type: "string",
              description: "Period",
              example: "2022-07-04",
            },
            value: {
              type: "string",
              description: "Assortment value",
              example: "1",
            },
          },
        },
      },
    },
  },
  responses: {
    200: {
      description: "OK",
      content: {
        "application/json": {
          schema: {
            type: "object",
            example: {
              success: true,
              data: [],
              meta: {
                retval: 1,
                serverTime: new Date(),
              },
            },
          },
        },
      },
    },
  },
};

const insertAssortmentSku = {
  tags: ["Default"],
  description: "Insert assortment value for the given sku",
  requestBody: {
    content: {
      "application/json": {
        schema: {
          type: "object",
          properties: {
            sku: {
              type: "string",
              description: "SKU",
              example: "194253124030",
            },
            date: {
              type: "string",
              description: "Period",
              example: "2022-07-04",
            },
            value: {
              type: "string",
              description: "Assortment value",
              example: "1",
            },
          },
        },
      },
    },
  },
  responses: {
    201: {
      description: "OK",
      content: {
        "application/json": {
          schema: {
            type: "object",
            example: {
              success: true,
              data: [],
              meta: {
                retval: 1,
                serverTime: new Date(),
              },
            },
          },
        },
      },
    },
  },
};

const defaultRouteDoc = {
  "/portool/api/add/sku": {
    post: addMultipleSku,
  },

  "/portool/api/delete/assortment/sku": {
    delete: deleteSku,
  },

  "/portool/api/update/assortment/sku": {
    put: updateAssortmentSku,
  },

  "/portool/api/insert/assortment/sku": {
    post: insertAssortmentSku,
  },
};

module.exports = defaultRouteDoc;
