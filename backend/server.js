const express = require("express");
const dotenv = require("dotenv").config();
const cors = require("cors");
const bodyParser = require("body-parser");
var sql = require("mssql/msnodesqlv8");
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerDocumentation = require("./helper/documentation");
const swaggerUi = require("swagger-ui-express");
const morgan = require("morgan");
const { format } = require("morgan");

const app = express();

// Using cors middleware
app.use(cors());

// parse application/x-www-form-urlencoded
// parse json
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Swagger setup
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocumentation));

// Using morgan
app.use(morgan("dev"));

// DB conncection
const config = {
  // user: "SVC_PRD_ScOpsBi_DeviceRepTeam",
  // password: "KT9:yG9TC9qU5hwk",
  server: "scopsbidb.internal.t-mobile.com",
  driver: "msnodesqlv8",
  database: "PSCO_dom",
  schema: "dbo",
  port: 63492,
  options: {
    encrypt: false, // for azure
    trustedConnection: true,
    useUTC: true,
  },
  pool: {
    max: 10,
    min: 0,
    idleTimeoutMillis: 60000,
  },
  requestTimeout: 180000,
};

sql.connect(config, function (err) {
  if (err) console.log(err);
  // create Request object
  console.log("DB connected");
});
let sqlRequest = new sql.Request();

// Routes

// To get the lockgroup dropdown list
app.get("/portool/api/device/lockgroup/list", (req, res) => {
  let list = [
    "Accessories",
    "IPHONE",
    "SUPER",
    "AFFORDABLE",
    "WEAR_UNIV",
    "TABLET",
    "HOTSPOT",
    "MID-SUPER",
    "DURABLE",
    "PHONE_FIRST",
    "IOT",
    "AFFORDABLE_HIGH",
    "NETWORK",
  ];
  if (list && list.length) {
    res.status(200).json({
      success: true,
      list: list,
      meta: {
        serverTime: new Date(),
        retVal: 1,
      },
    });
  }
});

// Device table list
app.get("/portool/api/device/table/list", (req, res) => {
  let sqlQuery =
    "Declare @SQL varchar(max) = 'Select * From (select r.SKU,Lockgroup,SubCategory,MaterialStandardName,DetailDescription,StandardCost,StatusName,Notes,CONVERT(VARCHAR,d.PORDate,120) as PORDate,a.AssortmentValue as Value from PSCO_dom.dbo.POR_Detail_Dates AS d left join PSCO_dom.dbo.POR_Assortment AS a on d.PORDate = a.Period right join PSCO_dom.dbo.POR_Detail_Devices AS r on a.SKU = r.SKU) src Pivot (sum(Value) For [PORDate] in (' + stuff((Select Distinct ',' + QuoteName(PORDate) From  PSCO_dom.dbo.POR_Detail_Dates Order By 1 For XML Path('') ),1,1,'')  + ') ) pvt' Exec(@SQL)";

  sqlRequest.query(sqlQuery, function (err, rows) {
    if (err) console.log(err);

    res.status(200).json({
      success: true,
      list: rows.recordset,
      // list: device,
      meta: {
        serverTime: new Date(),
        retVal: 1,
      },
    });
  });
});

// Protection table list
app.get("/portool/api/protection/table/list", (req, res) => {
  // query to the database and get the records
  let sqlQueryProtection =
    "Declare @SQL varchar(max) = 'Select * from (SELECT * FROM ( SELECT s.SKU, COALESCE(l.SKUDescr, m.MaterialStandardName) AS MaterialStandardName, CASE WHEN PrdGrp = '''+'HNDST'+''' THEN '''+'Device'+''' ELSE '''+'Accessory'+''' END AS ProductType, Department, COALESCE(b.DeviceSet1, b.DeviceSet2, b.DeviceSet3) AS DeviceSet, l.Class, e.Value AS StatusName, Notes, CONVERT(VARCHAR,CAST(d.FullDate AS DATE),120) as PORDate, a.AssortmentValue AS Value FROM ODS.dbo.Dim_Date AS d LEFT JOIN PSCO_dom.dbo.POR_Assortment AS a ON d.FullDate = a.[Period] AND a.SKU IS NOT NULL AND d.WeekDayName = '''+'Mon'+''' AND CAST(d.FullDate AS DATE) >= CAST(DATEADD(WK,DATEDIFF(WK,0,DATEADD(DD,-1,GETDATE())), 0) AS DATE) AND CAST(d.FullDate AS DATE) <= DATEADD(MONTH, 6, CAST(DATEADD(WK,DATEDIFF(WK,0,DATEADD(DD,-1,GETDATE())), 0) AS DATE)) RIGHT JOIN PSCO_dom.dbo.POR_SKU AS s ON s.SKU = a.SKU AND s.SKU IS NOT NULL LEFT JOIN ODS.dbo.APO_DP_Material AS b ON b.Item = s.SKU LEFT JOIN ( SELECT Product AS SKU FROM [ODS].[dbo].[APO_SNP_Material] WHERE [ImportFileDate] = (SELECT MAX(ImportFileDate) FROM ODS.dbo.P_Log WHERE Status = '''+'Success'+''' and Id = 2347) AND Product NOT IN ( SELECT m.MaterialNumber FROM ODS.dbo.BW_Material m INNER JOIN BusOps_PHP.dbo.MasterDeviceReference d ON m.Astockreference = d.Astockreference ) AND (MaterialGroup = '''+'Acc'+''' OR ZAPOModel = '''+'ACCESSORIES'+''' OR ZDPProdGroup = '''+'ACC'+''') UNION SELECT SKU FROM [ODS].[dbo].[RM_tblMap] WHERE SKUStatus NOT IN ('''+'Drop'+''','''+'DISC'+''','''+'DELIST'+''','''+'LIQ'+''') ) r ON r.SKU = s.SKU LEFT JOIN ODS.dbo.RM_tblMap l ON l.SKU = r.SKU LEFT JOIN ODS.dbo.BW_Material m ON m.MaterialNumber = r.SKU AND (MaterialStandardName NOT LIKE ('''+'%Sim%'+''') OR MaterialStandardName IS NULL) AND LEFT(r.SKU,3) <> '''+'ZZZ'+''' AND m.MaterialGroup NOT IN ('''+'MPREPAID'+''','''+'MOCK'+''','''+'SIM'+''','''+'MSIM'+''','''+'COL'+''','''+'SIMSB'+''','''+'OBS'+''','''+'SER'+''','''+'INACTIVE'+''','''+'DBCARD'+''','''+'MSIMSB'+''') LEFT JOIN BusOps_Promo.promo.Enumerations AS e ON e.Id = s.ForecastStatus WHERE s.Sheet = 2 ) A ) src Pivot (sum(Value) For [PORDate] in (' + stuff((Select Distinct ',' + QuoteName(PORDate) From PSCO_dom.dbo.POR_Detail_Dates Order By 1 For XML Path('') ),1,1,'') + ') ) pvt' Exec(@SQL)";

  sqlRequest.query(sqlQueryProtection, function (err, rows) {
    if (err) console.log(err);

    res.status(200).json({
      success: true,
      list: rows?.recordset,
      // list: protection,
      meta: {
        serverTime: new Date(),
        retVal: 1,
      },
    });
  });
});

// Universal table list
app.get("/portool/api/universal/table/list", (req, res) => {
  // let list = [];

  let sqlQuery =
    "Declare @SQL varchar(max) = 'Select * from (SELECT * FROM ( SELECT s.SKU, Department, l.Class, l.SubClass, COALESCE(l.SKUDescr, m.MaterialStandardName) AS MaterialStandardName, e.Value AS StatusName, Notes, CONVERT(VARCHAR,CAST(d.FullDate AS DATE),120) as PORDate, a.AssortmentValue AS Value FROM ODS.dbo.Dim_Date AS d LEFT JOIN PSCO_dom.dbo.POR_Assortment AS a ON d.FullDate = a.[Period] AND a.SKU IS NOT NULL AND d.WeekDayName = '''+'Mon'+''' AND CAST(d.FullDate AS DATE) >= CAST(DATEADD(WK,DATEDIFF(WK,0,DATEADD(DD,-1,GETDATE())), 0) AS DATE) AND CAST(d.FullDate AS DATE) <= DATEADD(MONTH, 6, CAST(DATEADD(WK,DATEDIFF(WK,0,DATEADD(DD,-1,GETDATE())), 0) AS DATE)) RIGHT JOIN PSCO_dom.dbo.POR_SKU AS s ON s.SKU = a.SKU AND s.SKU IS NOT NULL LEFT JOIN ( SELECT Product AS SKU FROM [ODS].[dbo].[APO_SNP_Material] WHERE [ImportFileDate] = (SELECT MAX(ImportFileDate) FROM ODS.dbo.P_Log WHERE Status = '''+'Success'+''' and Id = 2347) AND Product NOT IN ( SELECT m.MaterialNumber FROM ODS.dbo.BW_Material m INNER JOIN BusOps_PHP.dbo.MasterDeviceReference d ON m.Astockreference = d.Astockreference ) AND (MaterialGroup = '''+'Acc'+''' OR ZAPOModel = '''+'ACCESSORIES'+''' OR ZDPProdGroup = '''+'ACC'+''') UNION SELECT SKU FROM [ODS].[dbo].[RM_tblMap] WHERE SKUStatus NOT IN ('''+'Drop'+''','''+'DISC'+''','''+'DELIST'+''','''+'LIQ'+''') ) r ON r.SKU = s.SKU LEFT JOIN ODS.dbo.RM_tblMap l ON l.SKU = r.SKU LEFT JOIN ODS.dbo.BW_Material m ON m.MaterialNumber = r.SKU AND (MaterialStandardName NOT LIKE ('''+'%Sim%'+''') OR MaterialStandardName IS NULL) AND LEFT(r.SKU,3) <> '''+'ZZZ'+''' AND m.MaterialGroup NOT IN ('''+'MPREPAID'+''','''+'MOCK'+''','''+'SIM'+''','''+'MSIM'+''','''+'COL'+''','''+'SIMSB'+''','''+'OBS'+''','''+'SER'+''','''+'INACTIVE'+''','''+'DBCARD'+''','''+'MSIMSB'+''') LEFT JOIN BusOps_Promo.promo.Enumerations AS e ON e.Id = s.ForecastStatus WHERE s.Sheet = 1 ) A ) src Pivot (sum(Value) For [PORDate] in (' + stuff((Select Distinct ',' + QuoteName(PORDate) From PSCO_dom.dbo.POR_Detail_Dates Order By 1 For XML Path('') ),1,1,'') + ') ) pvt' Exec(@SQL)";
  // query to the database and get the records

  sqlRequest.query(sqlQuery, function (err, rows) {
    res.status(200).json({
      success: true,
      list: rows?.recordset,
      meta: {
        serverTime: new Date(),
        retVal: 1,
      },
    });
  });
});

// Adding multiple sku
app.post("/portool/api/add/sku", (req, res) => {
  const { tabName, skus, lockGroup } = req.body;

  let sheet = 3;
  let forecast = 10;
  if (tabName.includes("protection")) {
    sheet = 2;
  } else if (tabName.includes("universal")) {
    sheet = 1;
  } else if (tabName === "/") {
    sheet = 3;
  }

  let skusNotPresent = [];

  console.log(skus, "skus")
  for (const sku of skus) {
    let sqlAddSku =
      "INSERT INTO POR_SKU (SKU,Sheet,LockGroup,ForecastStatus) values ('" +
      sku.trim() +
      "','" +
      sheet +
      "','" +
      lockGroup +
      "','" +
      forecast +
      "')";

    sqlRequest.query(sqlAddSku, async (err, results) => {
      if (err) {
        if (err.message.includes("duplicate key")) {
          skusNotPresent.push(sku);
        } else {
          console.log(err);
          res.status(400).json({
            data: null,
            meta: {
              message: "Something went wrong",
              retval: 3,
              serverTime: new Date(),
            },
          });
        }
      }

      if (sku.trim() === skus[skus.length - 1].trim()) {
        if (skusNotPresent.length) {
          res.status(200).json({
            success: false,
            data: null,
            meta: {
              message: `SKU ${skusNotPresent.toString()} already exists`,
              retval: 2,
              serverTime: new Date(),
            },
          });
        } else {
          res.status(201).json({
            success: true,
            data: "SKU added",
            meta: {
              retval: 1,
              serverTime: new Date(),
            },
          });
        }
      }
    });
  }
});

app.delete("/portool/api/delete/sku", (req, res) => {
  const userId = req.query.sku;
  let sqlDelete1 =
    "DELETE from PSCO_dom.dbo.POR_SKU where SKU = '" + userId + "' ";
  let sqlDelete2 =
    "DELETE from PSCO_dom.dbo.POR_Assortment where SKU = '" + userId + "' ";
  try {
    sqlRequest.query(sqlDelete1, function (err, results) {
      if (err) console.log(err);
    });
    sqlRequest.query(sqlDelete2, function (err, results) {
      if (err) console.log(err);
    });
    res.status(200).json({
      success: true,
      data: null,
      meta: {
        retval: 1,
        serverTime: new Date(),
      },
    });
  } catch (e) {
    console.log(e);
  }
});

// Fetch sku details device
app.get("/portool/api/device/sku", (req, res) => {
  var userId = req.query.sku;
  if (userId) {
    let sqlEdit = `Declare @SQL varchar(max) = 'Select * From (select r.SKU as SKU,StatusName,Notes,CONVERT(VARCHAR,d.PORDate,120) as PORDate,a.AssortmentValue as Value from PSCO_dom.dbo.POR_Detail_Dates AS d left join PSCO_dom.dbo.POR_Assortment AS a on d.PORDate = a.Period right join PSCO_dom.dbo.POR_Detail_Devices AS r on a.SKU = r.SKU where r.SKU = '+'''${userId}'''+') src Pivot (sum(Value) For [PORDate] in (' + stuff((Select Distinct ',' + QuoteName(PORDate) From  PSCO_dom.dbo.POR_Detail_Dates Order By 1 For XML Path('') ),1,1,'')  + ') ) pvt' Exec(@SQL) `;
    sqlRequest.query(sqlEdit, function (err, result) {
      if (err) {
        console.log(err);
        res.status(400).json({
          success: false,
          data: null,
          meta: {
            retval: 3,
            serverTime: new Date(),
          },
        });
      } else if (result) {
        res.status(200).json({
          success: true,
          data: result.recordset,
          meta: {
            retval: 1,
            serverTime: new Date(),
          },
        });
      }
    });
  } else {
    res.status(404).json({
      success: false,
      data: "User id not found",
      meta: {
        retval: 4,
        serverTime: new Date(),
      },
    });
  }
});

// Fetch sku detail Protection
app.get("/portool/api/protection/sku", (req, res) => {
  var userId = req.query.sku;
  let sqlEditProtection = `Declare @SQL varchar(max) = 'Select * from (SELECT * FROM ( SELECT s.SKU, e.Value AS StatusName, Notes, CONVERT(VARCHAR,CAST(d.FullDate AS DATE),120) as PORDate, a.AssortmentValue AS Value FROM ODS.dbo.Dim_Date AS d LEFT JOIN PSCO_dom.dbo.POR_Assortment AS a ON d.FullDate = a.[Period] AND a.SKU IS NOT NULL AND d.WeekDayName = '''+'Mon'+''' AND CAST(d.FullDate AS DATE) >= CAST(DATEADD(WK,DATEDIFF(WK,0,DATEADD(DD,-1,GETDATE())), 0) AS DATE) AND CAST(d.FullDate AS DATE) <= DATEADD(MONTH, 6, CAST(DATEADD(WK,DATEDIFF(WK,0,DATEADD(DD,-1,GETDATE())), 0) AS DATE)) RIGHT JOIN PSCO_dom.dbo.POR_SKU AS s ON s.SKU = a.SKU AND s.SKU IS NOT NULL LEFT JOIN ODS.dbo.APO_DP_Material AS b ON b.Item = s.SKU LEFT JOIN ( SELECT Product AS SKU FROM [ODS].[dbo].[APO_SNP_Material] WHERE [ImportFileDate] = (SELECT MAX(ImportFileDate) FROM ODS.dbo.P_Log WHERE Status = '''+'Success'+''' and Id = 2347) AND Product NOT IN ( SELECT m.MaterialNumber FROM ODS.dbo.BW_Material m INNER JOIN BusOps_PHP.dbo.MasterDeviceReference d ON m.Astockreference = d.Astockreference ) AND (MaterialGroup = '''+'Acc'+''' OR ZAPOModel = '''+'ACCESSORIES'+''' OR ZDPProdGroup = '''+'ACC'+''') UNION SELECT SKU FROM [ODS].[dbo].[RM_tblMap] WHERE SKUStatus NOT IN ('''+'Drop'+''','''+'DISC'+''','''+'DELIST'+''','''+'LIQ'+''') ) r ON r.SKU = s.SKU LEFT JOIN ODS.dbo.RM_tblMap l ON l.SKU = r.SKU LEFT JOIN ODS.dbo.BW_Material m ON m.MaterialNumber = r.SKU AND (MaterialStandardName NOT LIKE ('''+'%Sim%'+''') OR MaterialStandardName IS NULL) AND LEFT(r.SKU,3) <> '''+'ZZZ'+''' AND m.MaterialGroup NOT IN ('''+'MPREPAID'+''','''+'MOCK'+''','''+'SIM'+''','''+'MSIM'+''','''+'COL'+''','''+'SIMSB'+''','''+'OBS'+''','''+'SER'+''','''+'INACTIVE'+''','''+'DBCARD'+''','''+'MSIMSB'+''') LEFT JOIN BusOps_Promo.promo.Enumerations AS e ON e.Id = s.ForecastStatus WHERE s.Sheet = 2 and s.SKU = '+'''${userId}'''+' ) A ) src Pivot (sum(Value) For [PORDate] in (' + stuff((Select Distinct ',' + QuoteName(PORDate) From PSCO_dom.dbo.POR_Detail_Dates Order By 1 For XML Path('') ),1,1,'') + ') ) pvt' Exec(@SQL)`;
  sqlRequest.query(sqlEditProtection, function (err, result) {
    if (err) console.log(err);

    res.status(200).json({
      data: result.recordset,
      meta: {
        retval: 1,
        serverTime: new Date(),
      },
    });
  });
});

// Fetch sku detail Universal
app.get("/portool/api/universal/sku", (req, res) => {
  var userId = req.query.sku;
  let sqlEditUniversal = `Declare @SQL varchar(max) = 'Select * from (SELECT * FROM ( SELECT s.SKU, e.Value AS StatusName, Notes, CONVERT(VARCHAR,CAST(d.FullDate AS DATE),120) as PORDate, a.AssortmentValue AS Value FROM ODS.dbo.Dim_Date AS d LEFT JOIN PSCO_dom.dbo.POR_Assortment AS a ON d.FullDate = a.[Period] AND a.SKU IS NOT NULL AND d.WeekDayName = '''+'Mon'+''' AND CAST(d.FullDate AS DATE) >= CAST(DATEADD(WK,DATEDIFF(WK,0,DATEADD(DD,-1,GETDATE())), 0) AS DATE) AND CAST(d.FullDate AS DATE) <= DATEADD(MONTH, 6, CAST(DATEADD(WK,DATEDIFF(WK,0,DATEADD(DD,-1,GETDATE())), 0) AS DATE)) RIGHT JOIN PSCO_dom.dbo.POR_SKU AS s ON s.SKU = a.SKU AND s.SKU IS NOT NULL LEFT JOIN ( SELECT Product AS SKU FROM [ODS].[dbo].[APO_SNP_Material] WHERE [ImportFileDate] = (SELECT MAX(ImportFileDate) FROM ODS.dbo.P_Log WHERE Status = '''+'Success'+''' and Id = 2347) AND Product NOT IN ( SELECT m.MaterialNumber FROM ODS.dbo.BW_Material m INNER JOIN BusOps_PHP.dbo.MasterDeviceReference d ON m.Astockreference = d.Astockreference ) AND (MaterialGroup = '''+'Acc'+''' OR ZAPOModel = '''+'ACCESSORIES'+''' OR ZDPProdGroup = '''+'ACC'+''') UNION SELECT SKU FROM [ODS].[dbo].[RM_tblMap] WHERE SKUStatus NOT IN ('''+'Drop'+''','''+'DISC'+''','''+'DELIST'+''','''+'LIQ'+''') ) r ON r.SKU = s.SKU LEFT JOIN ODS.dbo.RM_tblMap l ON l.SKU = r.SKU LEFT JOIN ODS.dbo.BW_Material m ON m.MaterialNumber = r.SKU AND (MaterialStandardName NOT LIKE ('''+'%Sim%'+''') OR MaterialStandardName IS NULL) AND LEFT(r.SKU,3) <> '''+'ZZZ'+''' AND m.MaterialGroup NOT IN ('''+'MPREPAID'+''','''+'MOCK'+''','''+'SIM'+''','''+'MSIM'+''','''+'COL'+''','''+'SIMSB'+''','''+'OBS'+''','''+'SER'+''','''+'INACTIVE'+''','''+'DBCARD'+''','''+'MSIMSB'+''') LEFT JOIN BusOps_Promo.promo.Enumerations AS e ON e.Id = s.ForecastStatus WHERE s.Sheet = 1 and s.SKU = '+'''${userId}'''+' ) A ) src Pivot (sum(Value) For [PORDate] in (' + stuff((Select Distinct ',' + QuoteName(PORDate) From PSCO_dom.dbo.POR_Detail_Dates Order By 1 For XML Path('') ),1,1,'') + ') ) pvt' Exec(@SQL)`;
  sqlRequest.query(sqlEditUniversal, function (err, result) {
    if (err) console.log(err);

    res.status(200).json({
      data: result.recordset,
      meta: {
        retval: 1,
        serverTime: new Date(),
      },
    });
  });
});

// Update sku
app.post("/portool/api/update/sku", (req, res) => {
  const { SKU, Notes, StatusName } = req.body;
  var forecaststatus;
  var mydata = Object.keys(req.body);

  if (StatusName == "Complete") {
    forecaststatus = 6;
  } else if (StatusName == "Update Needed") {
    forecaststatus = 10;
  } else if (StatusName == "Contingency") {
    forecaststatus = 7;
  } else if (StatusName == "Expired") {
    forecaststatus = 8;
  } else if (StatusName == "Cancelled") {
    forecaststatus = 61;
  } else if (StatusName == "Cancelled - SAS update Needed") {
    forecaststatus = 62;
  } else if (StatusName == "Part of Baseline Forecast") {
    forecaststatus = 63;
  } else {
    forecaststatus = 67; //Complete-DP Override
  }

  let sql3 =
    "UPDATE PSCO_dom.dbo.POR_SKU SET ForecastStatus='" +
    forecaststatus +
    "', Notes='" +
    Notes +
    "' where SKU = '" +
    SKU +
    "' ";
  console.log(sql3);
  sqlRequest.query(sql3, function (err, results) {
    if (err) console.log(err);
  });

  let assortmentValues = mydata.filter((dt) => {
    return !(dt == "Notes" || dt == "StatusName" || dt == "SKU");
  });
  let a = req.body;
  let userId = req.body.SKU;
  try {
    for (let i = 0; i < assortmentValues.length; i++) {
      var roww = [];
      var b = a[assortmentValues[i]];
      if (a[assortmentValues[i]] != "") {
        let sql8 =
          "Select * from PSCO_dom.dbo.POR_Assortment where SKU ='" +
          userId +
          "' and Period ='" +
          assortmentValues[i] +
          "' ";
        sqlRequest.query(sql8, function (err, row2) {
          if (err) console.log(err);
          roww = row2.recordset;
          console.log(roww);
          if (roww.length == 0) {
            let sql4 =
              "INSERT INTO PSCO_dom.dbo.POR_Assortment(SKU, Period, AssortmentValue) VALUES ('" +
              userId +
              "','" +
              assortmentValues[i] +
              "','" +
              a[assortmentValues[i]] +
              "')";
            sqlRequest.query(sql4, function (err, results2) {
              if (err) console.log(err);
              console.log(
                results2,
                a[assortmentValues[i]],
                userId,
                assortmentValues[i]
              );
            });
          } else {
            let sql5 =
              "UPDATE PSCO_dom.dbo.POR_Assortment SET AssortmentValue='" +
              a[assortmentValues[i]] +
              "' where SKU ='" +
              userId +
              "' and Period ='" +
              assortmentValues[i] +
              "' ";
            sqlRequest.query(sql5, function (err, results3) {
              if (err) console.log(err);
            });
          }
        });
      } else {
        let sql10 =
          "Select * from PSCO_dom.dbo.POR_Assortment where SKU ='" +
          userId +
          "' and Period ='" +
          assortmentValues[i] +
          "' ";

        sqlRequest.query(sql10, function (err, row3) {
          if (err) console.log(err);
          roww = row3.recordset;
          if (roww.length != 0) {
            let sql11 =
              "DELETE from PSCO_dom.dbo.POR_Assortment where SKU = '" +
              userId +
              "' and Period ='" +
              assortmentValues[i] +
              "' ";
            console.log(sql11);
            sqlRequest.query(sql11, function (err, results2) {
              if (err) console.log(err);
            });
          }
        });
      }
    }
    res.status(200).json({
      success: true,
      data: 1,
      meta: {
        retval: 1,
        serverTime: new Date(),
      },
    });
  } catch (e) {
    console.log(e);
  }
});

// Update assortment value for sku
app.put("/portool/api/update/assortment/sku", (req, res) => {
  const { sku, date, value } = req.body;
  let sqlMultiUpdate = ""
  if (date === "Notes") {
    sqlMultiUpdate = `UPDATE PSCO_dom.dbo.POR_SKU SET Notes= '${value}' WHERE SKU ='${sku}'`;
  } else if (date === "StatusName") {
    let forecaststatus = "";
    let StatusName = value;
    if (StatusName) {
      if (StatusName == "Complete") {
        forecaststatus = 6;
      } else if (StatusName == "Update Needed") {
        forecaststatus = 10;
      } else if (StatusName == "Contingency") {
        forecaststatus = 7;
      } else if (StatusName == "Expired") {
        forecaststatus = 8;
      } else if (StatusName == "Cancelled") {
        forecaststatus = 61;
      } else if (StatusName == "Cancelled - SAS update Needed") {
        forecaststatus = 62;
      } else if (StatusName == "Part of Baseline Forecast") {
        forecaststatus = 63;
      } else {
        forecaststatus = 67; //Complete-DP Override
      }

      sqlMultiUpdate = `UPDATE PSCO_dom.dbo.POR_SKU SET ForecastStatus= '${forecaststatus}' WHERE SKU ='${sku}'`;
    }
  }
  else {
    sqlMultiUpdate = `UPDATE PSCO_dom.dbo.POR_Assortment SET AssortmentValue= '${value}' WHERE SKU ='${sku}' AND Period = '${date}'`;
  }

  console.log(sqlMultiUpdate);
  try {
    sqlRequest.query(sqlMultiUpdate, function (err, data) {
      if (err) {
        console.log(err);
        res.status(400).send({
          data: null,
          meta: {
            retval: 3,
            serverTime: new Date(),
          },
        });
      } else {
        res.status(200).send({
          data: data,
          meta: {
            retval: 1,
            serverTime: new Date(),
          },
        });
      }
    });
  } catch (e) {
    console.log(e);
  }
});

// Inserting assortment value for the given sku
app.post("/portool/api/insert/assortment/sku", (req, res) => {
  const { sku, date, value } = req.body;

  let sqlMultiUpdateNew = "";
  if (date === "Notes") {
    sqlMultiUpdateNew = `UPDATE PSCO_dom.dbo.POR_SKU SET Notes= '${value}' WHERE SKU ='${sku}'`
  } else {
    sqlMultiUpdateNew = `INSERT INTO PSCO_dom.dbo.POR_Assortment(SKU, Period, AssortmentValue) VALUES (
      '${sku}',
      '${date}',
      '${value}')`;
  }

  try {
    sqlRequest.query(sqlMultiUpdateNew, function (err, data) {
      if (err) {
        console.log(err);
        res.status(400).send({
          data: null,
          meta: {
            retval: 3,
            serverTime: new Date(),
          },
        });
      } else {
        res.status(201).send({
          data: data,
          meta: {
            retval: 1,
            serverTime: new Date(),
          },
        });
      }
    });
  } catch (e) {
    console.log(e);
  }
});

// Delete period from sku
app.delete("/portool/api/delete/assortment/sku", (req, res) => {
  const { sku, date } = req.body;

  let sqlDelete = "";
  if (date === "Notes") {
    sqlDelete = `UPDATE PSCO_dom.dbo.POR_SKU SET Notes= '' WHERE SKU ='${sku}'`;
  } else {
    sqlDelete = `DELETE from PSCO_dom.dbo.POR_Assortment where SKU = '${sku}' and Period ='${date}'`;
  }


  try {
    sqlRequest.query(sqlDelete, function (err, data) {
      if (err) console.log(err);
      console.log(data);

      res.status(200).send({
        data: data,
        meta: {
          retval: 1,
          serverTime: new Date(),
        },
      });
    });
  } catch (e) {
    console.log(e);
  }
});

// Update sku details
app.put("/portool/api/update/sku/details", (req, res) => {
  const { StatusName, Notes, assortmentValues, sku } = req.body;
  console.log(req.body, "update");
  let forecaststatus;
  if (StatusName) {
    if (StatusName == "Complete") {
      forecaststatus = 6;
    } else if (StatusName == "Update Needed") {
      forecaststatus = 10;
    } else if (StatusName == "Contingency") {
      forecaststatus = 7;
    } else if (StatusName == "Expired") {
      forecaststatus = 8;
    } else if (StatusName == "Cancelled") {
      forecaststatus = 61;
    } else if (StatusName == "Cancelled - SAS update Needed") {
      forecaststatus = 62;
    } else if (StatusName == "Part of Baseline Forecast") {
      forecaststatus = 63;
    } else {
      forecaststatus = 67; //Complete-DP Override
    }

    let sqlUpdateFS =
      "UPDATE PSCO_dom.dbo.POR_SKU SET ForecastStatus='" +
      forecaststatus +
      "', Notes='" +
      Notes +
      "' where SKU = '" +
      sku +
      "' ";
    sqlRequest.query(sqlUpdateFS, function (err, results) {
      if (err) {
        console.log(err);
      } else if (!assortmentValues.length) {
        res.status(200).json({
          success: true,
          data: "Records updated successfuly",
          meta: {
            retval: 1,
            serverTime: new Date(),
          },
        });
      }
    });
  }

  if (assortmentValues?.length) {
    let isError = [];
    for (const value of assortmentValues) {
      let period = Object.keys(value)[0];
      let assortmentValue = Object.values(value)[0];
      let lastElement = Object.keys(
        assortmentValues[assortmentValues.length - 1]
      )[0];

      try {
        let sqlUpdateAssortment = `UPDATE PSCO_dom.dbo.POR_Assortment SET AssortmentValue= '${assortmentValue}' WHERE SKU ='${sku}' AND Period = '${period}'`;
        sqlRequest.query(sqlUpdateAssortment, function (err, data) {
          if (err) {
            console.log(err);
            isError.push(sku);
          } else {
            if (period.trim() === lastElement.trim()) {
              if (isError.length) {
                res.status(400).json({
                  success: false,
                  data: null,
                  meta: {
                    retval: 3,
                    serverTime: new Date(),
                  },
                });
              } else {
                res.status(200).json({
                  success: true,
                  data: "Records updated successfuly",
                  meta: {
                    retval: 1,
                    serverTime: new Date(),
                  },
                });
              }
            }
          }
        });
      } catch (e) {
        console.log(e);
      }
    }
  }
});

app.delete("/portool/api/delete/sku/details", (req, res) => {
  const { sku, assortmentValues, Notes } = req.body;

  console.log(req.body, "delete");
  if (Notes) {
    let sqlInsertNotes = `DELETE from PSCO_dom.dbo.POR_SKU where SKU = '${sku}' and Notes = 'Notes' `;
    sqlRequest.query(sqlInsertNotes, function (err, results) {
      if (err) {
        console.log(err);
      } else if (!assortmentValues.length) {
        res.status(200).json({
          success: true,
          data: "Records updated successfuly",
          meta: {
            retval: 1,
            serverTime: new Date(),
          },
        });
      }
    });
  }
  let isError = [];
  for (const period of assortmentValues) {
    console.log(period, "Period delete");
    let sqlDelete = `DELETE from PSCO_dom.dbo.POR_Assortment where SKU = '${sku}' and Period ='${period}'`;

    try {
      sqlRequest.query(sqlDelete, function (err, data) {
        if (err) {
          isError.push(sku);
          console.log(err);
        }
        else {
          if (period.trim() === assortmentValues[assortmentValues.length - 1]) {
            if (isError.length) {
              res.status(400).json({
                success: false,
                data: null,
                meta: {
                  retval: 3,
                  serverTime: new Date(),
                },
              });
            } else {
              res.status(200).json({
                success: true,
                data: "Records deleted successfuly",
                meta: {
                  retval: 1,
                  serverTime: new Date(),
                },
              });
            }
          }
        }


      });
    } catch (e) {
      console.log(e);
    }
  }
});

// Insert sku details
app.post("/portool/api/insert/sku/details", (req, res) => {
  const { Notes, assortmentValues, sku } = req.body;
  console.log(req.body, "Insert");

  if (assortmentValues?.length) {
    let isError = [];
    for (const value of assortmentValues) {
      let period = Object.keys(value)[0];
      let assortmentValue = Object.values(value)[0];
      let lastElement = Object.keys(
        assortmentValues[assortmentValues.length - 1]
      )[0];

      try {
        let sqlInsertAssortment =
          "INSERT INTO PSCO_dom.dbo.POR_Assortment(SKU, Period, AssortmentValue) VALUES ('" +
          sku +
          "','" +
          period +
          "','" +
          assortmentValue +
          "')";
        sqlRequest.query(sqlInsertAssortment, function (err, data) {
          if (err) {
            console.log(err);
            isError.push(sku);
          } else {
            if (period.trim() === lastElement.trim()) {
              if (isError.length) {
                res.status(400).json({
                  success: false,
                  data: null,
                  meta: {
                    retval: 3,
                    serverTime: new Date(),
                  },
                });
              } else {
                res.status(200).json({
                  success: true,
                  data: "Records inserted successfuly",
                  meta: {
                    retval: 1,
                    serverTime: new Date(),
                  },
                });
              }
            }
          }
        });
      } catch (e) {
        console.log(e);
      }
    }
  }
});

app.get("/portool/api/search/sku/bwmaterial", (req, res) => {
  // console.log(req.query.skus)

  let skus = req.query.skus;

  let formattedSkus = skus.map(el => `'${el}'`).join(",");

  console.log(formattedSkus)

  let searchBWMaterial = `SELECT MaterialNumber FROM [ODS].[dbo].[BW_Material] WHERE MaterialNumber IN (${formattedSkus})`;

  sqlRequest.query(searchBWMaterial, (err, data) => {
    if (err) {
      console.log(err)
    }
    else {
      let skusPresent = data?.recordset.map(dt => dt.MaterialNumber);
      let skusNotPresent = skus.filter(sk => !skusPresent.includes(sk));
      console.log(skusPresent, skusNotPresent)
      res.status(200).json({
        success: true,
        data: data.recordset,
        skusPresent: skusPresent,
        skusNotPresent: skusNotPresent,
        meta: {
          retval: 1,
          serverTime: new Date()
        }
      })
    }
  })
})

const port = process.env.PORT || 3001;

// Listening to port
app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
