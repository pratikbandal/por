const defaultRouteDoc = require("../routes/documentation/default.doc");
const deviceRouteDoc = require("../routes/documentation/device.doc");
const protectionRouteDoc = require("../routes/documentation/protection.doc");
const universalRouteDoc = require("../routes/documentation/universal.doc");

// Swagger Options
const swaggerDocumentation = {
  openapi: "3.0.0",
  info: {
    title: "POR Tool API",
    description: "POR Tool API Information",
    version: "1.0.0",
    contact: {
      email: "Pratik.Bandal1@T-Mobile.com",
    },
  },

  servers: [
    {
      url: "http://localhost:3007",
      description: "Local environment",
    },
    {
      url: "https://portool.com",
      description: "Prod environment",
    },
  ],
  apis: ["server.js"],

  tags: [
    {
      name: "Default",
      description: "Default routes",
    },
    {
      name: "Device",
      description: "Device routes",
    },
    {
      name: "Protection",
      description: "Protection routes",
    },
    {
      name: "Universal",
      description: "Universal routes",
    },
  ],

  paths: {
    ...defaultRouteDoc,
    ...deviceRouteDoc,
    ...protectionRouteDoc,
    ...universalRouteDoc
  },
};

module.exports = swaggerDocumentation;
